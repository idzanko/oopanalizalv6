﻿namespace AnalizaLV6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.aIN = new System.Windows.Forms.TextBox();
            this.bIN = new System.Windows.Forms.TextBox();
            this.reztext = new System.Windows.Forms.Label();
            this.rezultat = new System.Windows.Forms.Label();
            this.buttonplus = new System.Windows.Forms.Button();
            this.buttonputa = new System.Windows.Forms.Button();
            this.buttonminus = new System.Windows.Forms.Button();
            this.buttonpod = new System.Windows.Forms.Button();
            this.buttonsin = new System.Windows.Forms.Button();
            this.buttoncos = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.cIN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "a=";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "b=";
            // 
            // aIN
            // 
            this.aIN.Location = new System.Drawing.Point(36, 12);
            this.aIN.Name = "aIN";
            this.aIN.Size = new System.Drawing.Size(100, 20);
            this.aIN.TabIndex = 2;
            // 
            // bIN
            // 
            this.bIN.Location = new System.Drawing.Point(36, 35);
            this.bIN.Name = "bIN";
            this.bIN.Size = new System.Drawing.Size(100, 20);
            this.bIN.TabIndex = 3;
            // 
            // reztext
            // 
            this.reztext.AutoSize = true;
            this.reztext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.reztext.Location = new System.Drawing.Point(10, 170);
            this.reztext.Name = "reztext";
            this.reztext.Size = new System.Drawing.Size(90, 24);
            this.reztext.TabIndex = 4;
            this.reztext.Text = "Rezultat:";
            // 
            // rezultat
            // 
            this.rezultat.AutoSize = true;
            this.rezultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rezultat.Location = new System.Drawing.Point(102, 170);
            this.rezultat.Name = "rezultat";
            this.rezultat.Size = new System.Drawing.Size(0, 24);
            this.rezultat.TabIndex = 5;
            // 
            // buttonplus
            // 
            this.buttonplus.Location = new System.Drawing.Point(142, 12);
            this.buttonplus.Name = "buttonplus";
            this.buttonplus.Size = new System.Drawing.Size(30, 30);
            this.buttonplus.TabIndex = 7;
            this.buttonplus.Text = "+";
            this.buttonplus.UseVisualStyleBackColor = true;
            this.buttonplus.Click += new System.EventHandler(this.buttonplus_Click);
            // 
            // buttonputa
            // 
            this.buttonputa.Location = new System.Drawing.Point(142, 48);
            this.buttonputa.Name = "buttonputa";
            this.buttonputa.Size = new System.Drawing.Size(30, 30);
            this.buttonputa.TabIndex = 8;
            this.buttonputa.Text = "*";
            this.buttonputa.UseVisualStyleBackColor = true;
            this.buttonputa.Click += new System.EventHandler(this.buttonputa_Click);
            // 
            // buttonminus
            // 
            this.buttonminus.Location = new System.Drawing.Point(178, 12);
            this.buttonminus.Name = "buttonminus";
            this.buttonminus.Size = new System.Drawing.Size(30, 30);
            this.buttonminus.TabIndex = 9;
            this.buttonminus.Text = "-";
            this.buttonminus.UseVisualStyleBackColor = true;
            this.buttonminus.Click += new System.EventHandler(this.buttonminus_Click);
            // 
            // buttonpod
            // 
            this.buttonpod.Location = new System.Drawing.Point(178, 48);
            this.buttonpod.Name = "buttonpod";
            this.buttonpod.Size = new System.Drawing.Size(30, 30);
            this.buttonpod.TabIndex = 10;
            this.buttonpod.Text = "/";
            this.buttonpod.UseVisualStyleBackColor = true;
            this.buttonpod.Click += new System.EventHandler(this.buttonpod_Click);
            // 
            // buttonsin
            // 
            this.buttonsin.Location = new System.Drawing.Point(10, 120);
            this.buttonsin.Name = "buttonsin";
            this.buttonsin.Size = new System.Drawing.Size(35, 35);
            this.buttonsin.TabIndex = 11;
            this.buttonsin.Text = "sin";
            this.buttonsin.UseVisualStyleBackColor = true;
            this.buttonsin.Click += new System.EventHandler(this.buttonsin_Click);
            // 
            // buttoncos
            // 
            this.buttoncos.Location = new System.Drawing.Point(51, 120);
            this.buttoncos.Name = "buttoncos";
            this.buttoncos.Size = new System.Drawing.Size(35, 35);
            this.buttoncos.TabIndex = 12;
            this.buttoncos.Text = "cos";
            this.buttoncos.UseVisualStyleBackColor = true;
            this.buttoncos.Click += new System.EventHandler(this.buttoncos_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(92, 120);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(35, 35);
            this.button7.TabIndex = 13;
            this.button7.Text = "tan";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(133, 120);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(35, 35);
            this.button8.TabIndex = 14;
            this.button8.Text = "sqrt";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(173, 120);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(35, 35);
            this.button9.TabIndex = 15;
            this.button9.Text = "log";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // cIN
            // 
            this.cIN.Location = new System.Drawing.Point(36, 94);
            this.cIN.Name = "cIN";
            this.cIN.Size = new System.Drawing.Size(100, 20);
            this.cIN.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "c=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 214);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cIN);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.buttoncos);
            this.Controls.Add(this.buttonsin);
            this.Controls.Add(this.buttonpod);
            this.Controls.Add(this.buttonminus);
            this.Controls.Add(this.buttonputa);
            this.Controls.Add(this.buttonplus);
            this.Controls.Add(this.rezultat);
            this.Controls.Add(this.reztext);
            this.Controls.Add(this.bIN);
            this.Controls.Add(this.aIN);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox aIN;
        private System.Windows.Forms.TextBox bIN;
        private System.Windows.Forms.Label reztext;
        private System.Windows.Forms.Label rezultat;
        private System.Windows.Forms.Button buttonplus;
        private System.Windows.Forms.Button buttonputa;
        private System.Windows.Forms.Button buttonminus;
        private System.Windows.Forms.Button buttonpod;
        private System.Windows.Forms.Button buttonsin;
        private System.Windows.Forms.Button buttoncos;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox cIN;
        private System.Windows.Forms.Label label3;
    }
}

