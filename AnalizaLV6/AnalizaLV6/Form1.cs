﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalizaLV6
{
    public partial class Form1 : Form
    {
        public double a=0, b=0, c=0;

        public bool provjera() //izvrsava provjeru
        {
            bool empty = false;
            

            if (aIN.Text == "" || bIN.Text == "") //ako broj a ili b nisu upisani izbacuje pogresku
            {
                empty = true;
                MessageBox.Show("Niste upisali a ili b", "Pogreška!");
                return false;
            }
            else
            {
                empty = false;
                if (!double.TryParse(aIN.Text, out a))  //ako je umjesto broja upisano npr. slovo ispisuje pogresku
                {
                    MessageBox.Show("Pogrešan unos broja a.", "Pogreška!");
                    return false;
                }

                if (!double.TryParse(bIN.Text, out b))  //ako je umjesto broja upisano npr. slovo ispisuje pogresku
                {
                    MessageBox.Show("Pogrešan unos broja b.", "Pogreška!");
                    return false;
                }

                return true; //ako je sve ispravno ne ispisuje pogresku
            }
        }

        public bool provjera2() //izvrsava provjeru za napredne operacije
        {
            bool empty = false;

            if (cIN.Text == "" ) //ako broj c nije upisan izbacuje pogresku
            {
                empty = true;
                MessageBox.Show("Niste upisali c", "Pogreška!");
                return false;
            }
            else
            {
                empty = false;
                if (!double.TryParse(cIN.Text, out c))  //ako je umjesto broja upisano npr. slovo ispisuje pogresku
                {
                    MessageBox.Show("Pogrešan unos broja c.", "Pogreška!");
                    return false;
                }
                return true; //ako je sve ispravno ne ispisuje pogresku
            }
        }


        private void buttonminus_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera() == true) //Ako je sve ispravno uneseno izvrsi operaciju
                rezultat.Text = (a - b).ToString(); //ispis rezultata
        }

        private void buttonputa_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera() == true) //Ako je sve ispravno uneseno izvrsi operaciju
                rezultat.Text = (a * b).ToString(); //ispis rezultata
        }

        private void buttonsin_Click(object sender, EventArgs e)
        {

            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera2() == true) //Ako je sve ispravno uneseno izvrsi operaciju
            {
                c=Math.PI* c / 180; //pretvara radijane u stupnjeve
                rezultat.Text = (Math.Sin(c)).ToString(); //ispis rezultata

            }
        }
        private void buttonpod_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera() == true) //Ako je sve ispravno uneseno izvrsi operaciju
                rezultat.Text = (a / b).ToString(); //ispis rezultata
        }

        private void button7_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera2() == true) //Ako je sve ispravno uneseno izvrsi operaciju
            {
                c = Math.PI * c / 180; //pretvara radijane u stupnjeve
                rezultat.Text = (Math.Tan(c)).ToString(); //ispis rezultata

            }
        }

        private void buttoncos_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera2() == true) //Ako je sve ispravno uneseno izvrsi operaciju
            {
                c = Math.PI * c / 180; //pretvara radijane u stupnjeve
                rezultat.Text = (Math.Cos(c)).ToString(); //ispis rezultata

            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera2() == true) //Ako je sve ispravno uneseno izvrsi operaciju
            {
             rezultat.Text = (Math.Sqrt(c)).ToString(); //ispis rezultata

            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera2() == true) //Ako je sve ispravno uneseno izvrsi operaciju
            {
                rezultat.Text = (Math.Log10(c)).ToString(); //ispis rezultata

            }
        }

        private void buttonplus_Click(object sender, EventArgs e)
        {
            rezultat.Text = "";     //vraca rezultat na prazno
            if (provjera()==true) //Ako je sve ispravno uneseno izvrsi operaciju
                rezultat.Text = (a + b).ToString(); //ispis rezultata
        }

        public Form1()
        {
            InitializeComponent();
        }

        
    }
}
