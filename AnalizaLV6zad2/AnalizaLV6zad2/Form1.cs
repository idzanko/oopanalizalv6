﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalizaLV6zad2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        List<Pojam> listPojmovi = new List<Pojam>();
        string path = "D:\\pojmovi.txt";


        private void button1_Click(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split('\n');
                    Pojam P = new Pojam(parts[0]);
                    listPojmovi.Add(P);
                }
            }
        }

        class Pojam
        {
            #region data_members
            private string pojamt;
            #endregion
            #region public_methods
            public Pojam()
            {
                pojamt = "default";
            }
            public Pojam(string pt)
            {
                pojamt = pt;
            }
            public override string ToString()
            {
                return pojamt.ToString();
            }

            #endregion
        }

        private void pogodi_Click(object sender, EventArgs e)
        {
           
        }

        private void igraj_Click(object sender, EventArgs e)
        {

        }
    }
}
