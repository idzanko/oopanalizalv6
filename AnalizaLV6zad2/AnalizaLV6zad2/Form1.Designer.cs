﻿namespace AnalizaLV6zad2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_char1 = new System.Windows.Forms.Label();
            this.lb_char2 = new System.Windows.Forms.Label();
            this.lb_char3 = new System.Windows.Forms.Label();
            this.lb_char4 = new System.Windows.Forms.Label();
            this.lb_char5 = new System.Windows.Forms.Label();
            this.pogodi = new System.Windows.Forms.Button();
            this.igraj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_preostalozivota = new System.Windows.Forms.Label();
            this.tb_preostalozivota = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lb_char1
            // 
            this.lb_char1.AutoSize = true;
            this.lb_char1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_char1.Location = new System.Drawing.Point(119, 9);
            this.lb_char1.Name = "lb_char1";
            this.lb_char1.Size = new System.Drawing.Size(16, 20);
            this.lb_char1.TabIndex = 0;
            this.lb_char1.Text = "*";
            // 
            // lb_char2
            // 
            this.lb_char2.AutoSize = true;
            this.lb_char2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_char2.Location = new System.Drawing.Point(141, 9);
            this.lb_char2.Name = "lb_char2";
            this.lb_char2.Size = new System.Drawing.Size(16, 20);
            this.lb_char2.TabIndex = 1;
            this.lb_char2.Text = "*";
            // 
            // lb_char3
            // 
            this.lb_char3.AutoSize = true;
            this.lb_char3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_char3.Location = new System.Drawing.Point(163, 9);
            this.lb_char3.Name = "lb_char3";
            this.lb_char3.Size = new System.Drawing.Size(16, 20);
            this.lb_char3.TabIndex = 2;
            this.lb_char3.Text = "*";
            // 
            // lb_char4
            // 
            this.lb_char4.AutoSize = true;
            this.lb_char4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_char4.Location = new System.Drawing.Point(185, 9);
            this.lb_char4.Name = "lb_char4";
            this.lb_char4.Size = new System.Drawing.Size(16, 20);
            this.lb_char4.TabIndex = 3;
            this.lb_char4.Text = "*";
            // 
            // lb_char5
            // 
            this.lb_char5.AutoSize = true;
            this.lb_char5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_char5.Location = new System.Drawing.Point(207, 9);
            this.lb_char5.Name = "lb_char5";
            this.lb_char5.Size = new System.Drawing.Size(16, 20);
            this.lb_char5.TabIndex = 4;
            this.lb_char5.Text = "*";
            // 
            // pogodi
            // 
            this.pogodi.Location = new System.Drawing.Point(263, 43);
            this.pogodi.Name = "pogodi";
            this.pogodi.Size = new System.Drawing.Size(75, 23);
            this.pogodi.TabIndex = 5;
            this.pogodi.Text = "Pogodi";
            this.pogodi.UseVisualStyleBackColor = true;
            this.pogodi.Click += new System.EventHandler(this.pogodi_Click);
            // 
            // igraj
            // 
            this.igraj.Location = new System.Drawing.Point(344, 43);
            this.igraj.Name = "igraj";
            this.igraj.Size = new System.Drawing.Size(75, 23);
            this.igraj.TabIndex = 6;
            this.igraj.Text = "Igraj";
            this.igraj.UseVisualStyleBackColor = true;
            this.igraj.Click += new System.EventHandler(this.igraj_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Preostalo života:";
            // 
            // lb_preostalozivota
            // 
            this.lb_preostalozivota.AutoSize = true;
            this.lb_preostalozivota.Location = new System.Drawing.Point(103, 50);
            this.lb_preostalozivota.Name = "lb_preostalozivota";
            this.lb_preostalozivota.Size = new System.Drawing.Size(35, 13);
            this.lb_preostalozivota.TabIndex = 8;
            this.lb_preostalozivota.Text = "label2";
            // 
            // tb_preostalozivota
            // 
            this.tb_preostalozivota.Location = new System.Drawing.Point(157, 43);
            this.tb_preostalozivota.Name = "tb_preostalozivota";
            this.tb_preostalozivota.Size = new System.Drawing.Size(100, 20);
            this.tb_preostalozivota.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Ucitaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 84);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tb_preostalozivota);
            this.Controls.Add(this.lb_preostalozivota);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.igraj);
            this.Controls.Add(this.pogodi);
            this.Controls.Add(this.lb_char5);
            this.Controls.Add(this.lb_char4);
            this.Controls.Add(this.lb_char3);
            this.Controls.Add(this.lb_char2);
            this.Controls.Add(this.lb_char1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_char1;
        private System.Windows.Forms.Label lb_char2;
        private System.Windows.Forms.Label lb_char3;
        private System.Windows.Forms.Label lb_char4;
        private System.Windows.Forms.Label lb_char5;
        private System.Windows.Forms.Button pogodi;
        private System.Windows.Forms.Button igraj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_preostalozivota;
        private System.Windows.Forms.TextBox tb_preostalozivota;
        private System.Windows.Forms.Button button1;
    }
}

